import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BlendedSpicesComponent } from './blended-spices.component';

describe('BlendedSpicesComponent', () => {
  let component: BlendedSpicesComponent;
  let fixture: ComponentFixture<BlendedSpicesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BlendedSpicesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BlendedSpicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
