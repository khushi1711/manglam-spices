import { BrowserModule } from '@angular/platform-browser';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import {Routes,RouterModule} from '@angular/router';
import { AppRoutingModule} from './app-routing.module';
import { AppComponent } from './app.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { HttpClientModule} from '@angular/common/http';
import { HomeComponent } from './home/home.component';
import { ProductsComponent } from './products/products.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { RecipiesComponent } from './recipies/recipies.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {MatCardModule} from '@angular/material/card';
import {NgImageSliderModule} from 'ng-image-slider';
import { MatToolbarModule } from "@angular/material/toolbar";
import { MatSidenavModule } from "@angular/material/sidenav";
import {MatButtonModule} from "@angular/material/button";
 import {MatMenuModule} from "@angular/material/menu";
 import {MatIconModule} from "@angular/material/icon";
 import { BrowserAnimationsModule} from "@angular/platform-browser/animations";
 import { FlexLayoutModule } from "@angular/flex-layout";
import { GroundSpicesComponent } from './ground-spices/ground-spices.component';
import { BlendedSpicesComponent } from './blended-spices/blended-spices.component';
import {MatSliderModule} from '@angular/material/slider';
import { BuyComponent } from './buy/buy.component';

const routes: Routes = [
{path:"home",component:HomeComponent},
{path:"products",component:ProductsComponent},
{path:"products/ground-spices",component:GroundSpicesComponent},
{path:"products/blended-spices",component:BlendedSpicesComponent},
{path:"contact-us",component:ContactUsComponent},
{path:"about-us",component:AboutUsComponent},
{path:"Recipies",component:RecipiesComponent},
{path:"ground-spices",component:GroundSpicesComponent},
{path:"blended-spices",component:BlendedSpicesComponent},
{path:"buy",component:BuyComponent},
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ProductsComponent,
    ContactUsComponent,
    RecipiesComponent,
    AboutUsComponent,
    GroundSpicesComponent,
    BlendedSpicesComponent,
    BuyComponent,
    
    
 ],

  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
    NgbModule,
    NgImageSliderModule,
    MatMenuModule,
    MatToolbarModule,
    MatIconModule,
    MatSidenavModule,
    MatButtonModule,
    MatCardModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    MatSliderModule,
    FontAwesomeModule
  ],
  schemas:[CUSTOM_ELEMENTS_SCHEMA],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}

