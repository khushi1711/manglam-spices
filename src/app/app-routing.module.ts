import { NgModule } from '@angular/core';
import { GroundSpicesComponent } from './ground-spices/ground-spices.component';
import { BlendedSpicesComponent } from './blended-spices/blended-spices.component';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'products', pathMatch: 'full' },
  { path: 'GROUND-SPICES', component:GroundSpicesComponent},
  { path: 'BLENDED-SPICES', component: BlendedSpicesComponent}
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

  