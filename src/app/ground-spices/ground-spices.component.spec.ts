import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GroundSpicesComponent } from './ground-spices.component';

describe('GroundSpicesComponent', () => {
  let component: GroundSpicesComponent;
  let fixture: ComponentFixture<GroundSpicesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GroundSpicesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GroundSpicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
